# Fullstack developer Test

Create a simple app where a user can add and view brief notes.

Each note should just have a **title** and **content**.

**Rules**
- You can use any library you wish.
- You are also allowed to use the internet.
- You are not allowed to use ChatGPT or any other AI related tools.

## Frontend (React)
- **Create a new React app** using vite or your preferred setup.
- **Add Note:**
  - Implement a simple form to add a new note.
- **List Notes:**
  - Display a list of notes.
  - Newly added notes should appear at the top.

## Backend (Node.js)
- **Initialize a new Node.js project** using Express or your preferred framework.
- **In-memory Storage:**
  - Use an Array to store notes for simplicity.
